package com.boot.bookingrestaurantapi.services;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.boot.bookingrestaurantapi.entities.Reservation;
import com.boot.bookingrestaurantapi.entities.Restaurant;
import com.boot.bookingrestaurantapi.entities.Turn;
import com.boot.bookingrestaurantapi.exceptions.BookingException;
import com.boot.bookingrestaurantapi.jsons.CreateReservationRest;
import com.boot.bookingrestaurantapi.repositories.ReservationRespository;
import com.boot.bookingrestaurantapi.repositories.RestaurantRepository;
import com.boot.bookingrestaurantapi.repositories.TurnRepository;
import com.boot.bookingrestaurantapi.services.impl.ReservationServiceImpl;

public class ReservationServiceTest {

	private static final Long RESERVATION_ID = 4L;
	private static final Date RESERVATION_DATE = new Date();
	private static final Long RESERVATION_PERSON = 30L;
	private static final Long RESERVATION_RESTAURANT_ID = 4L;
	private static final Long RESERVATION_TURN_ID = 5L;
	private static final String RESERVATION_TURN = "Turn_12_004";
	private static final String RESERVATION_LOCATOR = "BURGER_3";
	
	
	private static final Long RESTAURANT_ID = 4L;
	private static final String RESTAURANT_NAME = "Buger";
	private static final String RESTAURANT_DESCRIPTION = "Todo tipo de burger";
	private static final String RESTAURANT_ADDRESS = "Av. Luro";
	private static final String RESTAURANT_IMAGE = "www.image.com";
	public static final List<Turn> TURN_LIST = new ArrayList<>();
	private static final Restaurant RESTAURANT = new Restaurant();
	private static final Optional<Restaurant> OPTIONAL_RESTAURANT = Optional.of(RESTAURANT);

	
	private static final Long TURN_ID = 5L;
	private static final String TURN_NAME = "Turno para 9";
	private static final Turn TURN = new Turn();
	private static final Optional<Turn> OPTIONAL_TURN = Optional.of(TURN);
	
	CreateReservationRest CREATE_RESERVATION_REST = new CreateReservationRest();
	
	private static final Optional<Reservation> OPTIONAL_RESERVATION_EMPTY = Optional.empty();
	
	private static final Optional<Restaurant> OPTIONAL_RESTAURANT_EMPTY = Optional.empty();
	
	private static final Optional<Turn> OPTIONAL_TURN_EMPTY = Optional.empty();
	
	
	
	private static final Reservation RESERVATION = new Reservation();
	private static final Optional<Reservation> OPTIONAL_RESERVATION = Optional.of(RESERVATION);
	
	
	
	
	@Mock
	private RestaurantRepository restaurantRepository;

	@Mock
	private TurnRepository turnRepository;

	@Mock
	private ReservationRespository reservationRepository;

	@InjectMocks
	private ReservationServiceImpl reservationService;

	@Before
	public void init() throws BookingException {
		MockitoAnnotations.initMocks(this);

		RESTAURANT.setId(RESTAURANT_ID);
		RESTAURANT.setName(RESTAURANT_NAME);
		RESTAURANT.setDescription(RESTAURANT_DESCRIPTION);
		RESTAURANT.setAddress(RESTAURANT_ADDRESS);
		RESTAURANT.setImage(RESTAURANT_IMAGE);
		RESTAURANT.setTurns(TURN_LIST);
		
		TURN.setId(TURN_ID);
		TURN.setName(TURN_NAME);
		TURN.setRestaurant(RESTAURANT);

		CREATE_RESERVATION_REST.setDate(RESERVATION_DATE);
		CREATE_RESERVATION_REST.setPerson(RESERVATION_PERSON);
		CREATE_RESERVATION_REST.setRestaurantId(RESERVATION_RESTAURANT_ID);
		CREATE_RESERVATION_REST.setTurnId(RESERVATION_TURN_ID);
		
		
		RESERVATION.setId(RESERVATION_ID);
		RESERVATION.setDate(RESERVATION_DATE);
		RESERVATION.setLocator(RESERVATION_LOCATOR);
		RESERVATION.setTurn(RESERVATION_TURN);
		RESERVATION.setPerson(RESERVATION_PERSON);
		RESERVATION.setRestaurant(RESTAURANT);
	}

	@Test
	public void getReservationTest() throws BookingException {
		Mockito.when(reservationRepository.findById(RESERVATION_ID)).thenReturn(Optional.of(RESERVATION));
		reservationService.getReservation(RESERVATION_ID);
	}
	
	@Test(expected = BookingException.class)
	public void getReservationTestError() throws BookingException {
		Mockito.when(reservationRepository.findById(RESERVATION_ID)).thenReturn(Optional.empty());
		reservationService.getReservation(RESERVATION_ID);
		fail();
	}
	
	@Test
	public void createReservationTest() throws BookingException {
		Mockito.when(restaurantRepository.findById(RESERVATION_RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
		Mockito.when(turnRepository.findById(RESERVATION_TURN_ID)).thenReturn(OPTIONAL_TURN);
		Mockito.when(reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId())).thenReturn(OPTIONAL_RESERVATION_EMPTY);
		Mockito.when(reservationRepository.save(Mockito.any(Reservation.class))).thenReturn(new Reservation());
		
		reservationService.createReservation(CREATE_RESERVATION_REST);
	}
	
	@Test(expected = BookingException.class)
	public void createReservationRestaurantFindByIdTestError() throws BookingException {
		Mockito.when(restaurantRepository.findById(RESERVATION_RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT_EMPTY);
		
		reservationService.createReservation(CREATE_RESERVATION_REST);
		fail();
	}
	
	@Test(expected = BookingException.class)
	public void createReservationTurnFindByIdTestError() throws BookingException {
		Mockito.when(restaurantRepository.findById(RESERVATION_RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
		Mockito.when(turnRepository.findById(RESERVATION_TURN_ID)).thenReturn(OPTIONAL_TURN_EMPTY);
		
		reservationService.createReservation(CREATE_RESERVATION_REST);
		fail();
	}
	
	@Test(expected = BookingException.class)
	public void createReservationFindByTurnAndRestaurantIdTestError() throws BookingException {
		Mockito.when(restaurantRepository.findById(RESERVATION_RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
		Mockito.when(turnRepository.findById(RESERVATION_TURN_ID)).thenReturn(OPTIONAL_TURN);
		Mockito.when(reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId())).thenReturn(OPTIONAL_RESERVATION);
		
		reservationService.createReservation(CREATE_RESERVATION_REST);
		fail();
	}
	
	@Test(expected = BookingException.class)
	public void createReservationInternalServerErrorExceptionTest() throws BookingException {
		Mockito.when(restaurantRepository.findById(RESERVATION_RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
		Mockito.when(turnRepository.findById(RESERVATION_TURN_ID)).thenReturn(OPTIONAL_TURN);
		Mockito.when(reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId())).thenReturn(OPTIONAL_RESERVATION_EMPTY);
		
		Mockito.doThrow(Exception.class).when(reservationRepository).save(Mockito.any(Reservation.class));
		
		
		reservationService.createReservation(CREATE_RESERVATION_REST);
		fail();
	}
	

}
