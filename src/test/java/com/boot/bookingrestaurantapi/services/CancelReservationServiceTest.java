package com.boot.bookingrestaurantapi.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.boot.bookingrestaurantapi.entities.Reservation;
import com.boot.bookingrestaurantapi.exceptions.BookingException;
import com.boot.bookingrestaurantapi.repositories.ReservationRespository;
import com.boot.bookingrestaurantapi.services.impl.CancelReservationServiceImpl;

public class CancelReservationServiceTest {

	private static final String RESERVATION_DELETED = "LOCATOR_DELETED";

	private static final String LOCATOR = "Burger 7";

	private static final Reservation RESERVATION = new Reservation();
	private static final Optional<Reservation> OPTIONAL_RESERVATION = Optional.of(RESERVATION);

	private static final Optional<Reservation> OPTIONAL_RESERVATION_EMPTY = Optional.empty();

	@Mock
	private ReservationRespository reservationRespository;

	@InjectMocks
	private CancelReservationServiceImpl cancelReservationService;

	@Before
	public void init() throws BookingException {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void deleteReservationTest() throws BookingException {
		Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(OPTIONAL_RESERVATION);

		Mockito.when(reservationRespository.deleteByLocator(LOCATOR)).thenReturn(OPTIONAL_RESERVATION);

		final String response = cancelReservationService.deleteReservation(LOCATOR);
		assertEquals(response, RESERVATION_DELETED);

	}

	@Test(expected = BookingException.class)
	public void deleteReservationFindByLocatorTestError() throws BookingException {
		Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(OPTIONAL_RESERVATION_EMPTY);

		cancelReservationService.deleteReservation(LOCATOR);
		fail();
	}

	@Test(expected = BookingException.class)
	public void deleteReservationInternalServerErrorExceptionTest() throws BookingException {

		Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(OPTIONAL_RESERVATION);

		Mockito.doThrow(Exception.class).when(reservationRespository).deleteByLocator(LOCATOR);

		cancelReservationService.deleteReservation(LOCATOR);
		fail();
	}

}
