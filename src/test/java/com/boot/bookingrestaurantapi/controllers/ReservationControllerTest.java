package com.boot.bookingrestaurantapi.controllers;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.boot.bookingrestaurantapi.exceptions.BookingException;
import com.boot.bookingrestaurantapi.jsons.CreateReservationRest;
import com.boot.bookingrestaurantapi.jsons.ReservationRest;
import com.boot.bookingrestaurantapi.jsons.RestaurantRest;
import com.boot.bookingrestaurantapi.responses.BookingResponse;
import com.boot.bookingrestaurantapi.services.ReservationService;

public class ReservationControllerTest {
	
	private static final String SUCCES_STATUS = "Succes";
	private static final String SUCCES_CODE = "200 OK";
	private static final String OK = "OK";
	
	
	private static final Long RESERVATION_ID = 1L;
	private static final Date RESERVATION_DATE = new Date();
	private static final Long RESERVATION_PERSON = 1L;
	private static final Long RESERVATION_RESTAURANT_ID = 1L;
	private static final Long RESERVATION_TURN_ID = 1L;
	
	private static final String RESERVATION_LOCATOR = "BURGER 2";
	
	public static final ReservationRest RESERVATION_REST = new ReservationRest();
	
	CreateReservationRest CREATE_RESERVATION_REST = new CreateReservationRest();
	
	
	@Mock
	ReservationService reservationService;

	@InjectMocks
	ReservationController reservationController;

	@Before
	public void init() throws BookingException {
		MockitoAnnotations.initMocks(this);
		
		RESERVATION_REST.setRestaurantId(RESERVATION_ID);
		RESERVATION_REST.setDate(RESERVATION_DATE);
		RESERVATION_REST.setPerson(RESERVATION_PERSON);
		RESERVATION_REST.setRestaurantId(RESERVATION_RESTAURANT_ID);
		RESERVATION_REST.setTurnId(RESERVATION_TURN_ID);
		RESERVATION_REST.setLocator(RESERVATION_LOCATOR);
		Mockito.when(reservationService.getReservation(RESERVATION_ID)).thenReturn(RESERVATION_REST);

		CREATE_RESERVATION_REST.setRestaurantId(RESERVATION_ID);
		CREATE_RESERVATION_REST.setDate(RESERVATION_DATE);
		CREATE_RESERVATION_REST.setPerson(RESERVATION_PERSON);
		CREATE_RESERVATION_REST.setRestaurantId(RESERVATION_RESTAURANT_ID);
		CREATE_RESERVATION_REST.setTurnId(RESERVATION_TURN_ID);
		Mockito.when(reservationService.createReservation(CREATE_RESERVATION_REST)).thenReturn(RESERVATION_LOCATOR);
		
		
	}
	
	
	@Test
	public void getReservationByIdTest() throws BookingException {
		final BookingResponse<ReservationRest> response = reservationController.getReservationById(RESERVATION_ID);
		assertEquals(response.getStatus(), SUCCES_STATUS);
		assertEquals(response.getCode(), SUCCES_CODE);
		assertEquals(response.getMessage(), OK);
		assertEquals(response.getData(), RESERVATION_REST);
	}
	
	@Test
	public void createReservationTest() throws BookingException {
		final BookingResponse<String> response = reservationController.createReservation(CREATE_RESERVATION_REST);
		assertEquals(response.getStatus(), SUCCES_STATUS);
		assertEquals(response.getCode(), SUCCES_CODE);
		assertEquals(response.getMessage(), OK);
		assertEquals(response.getData(), RESERVATION_LOCATOR);
		
	}
	
	

}
